@extends('layouts.cp')
@section('title', 'Services')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="pull-left">
      <h2>Services</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-success" href="#"> Create New Service</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
@endif

<table class="table table-bordered table-striped table-hover">
  <tr>
    <th>No</th>
    <th>ID</th>
    <th width="300px">Title</th>
    <th>Description</th>
    <th width="200px">Action</th>
  </tr>
  @foreach ($services as $service)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $service->id}}</td>
    <td>{{ $service->title }}</td>
    <td>{{ $service->description }}</td>
    <td>
     
    </td>
  </tr>
  @endforeach
</table>

@endsection
