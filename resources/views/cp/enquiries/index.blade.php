@extends('layouts.cp')
@section('title', 'Enquiries')
@section('content')
    <div class="container">
        <h1 style="font-size: 2.2rem">Enquiries</h1>
        <hr/>
        {!! Form::open(['method'=>'get']) !!}
        <div class="row">
            <div class="col-sm-5 form-group">
                <div class="input-group">
                    <input class="form-control" id="search" value="{{ request('search') }}" placeholder="Search name" name="search" type="text" />
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-warning">Search</button>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{request('field')}}" name="field"/>
            <input type="hidden" value="{{request('sort')}}" name="sort"/>
        </div>
        {!! Form::close() !!}
        <table class="table table-bordered bg-light">
            <thead class="bg-primary" style="color: white">
            <tr>
                <th width="60px" style="vertical-align: middle;text-align: center">ID</th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/enquiries')}}?search={{request('search')}}&field=name&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Name
                    </a>
                    {{request('field','name')=='name'?(request('sort','asc')=='asc'?' ':'&#9662;'):''}}
                </th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/enquiries')}}?search={{request('search')}}&field=email&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Email
                    </a>
                    {{request('field')=='email'?(request('sort','asc')=='asc'?'&#9652;':'&#9662;'):''}}
                </th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/enquiries')}}?search={{request('search')}}&field=mobile&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Mobile
                    </a>
                    {{request('field')=='email'?(request('sort','asc')=='asc'?'&#9652;':'&#9662;'):''}}
                </th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/enquiries')}}?search={{request('search')}}&field=message&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Message
                    </a>
                    {{request('field')=='message'?(request('sort','asc')=='asc'?'&#9652;':'&#9662;'):''}}
                </th>

                <th width="130px" style="vertical-align: middle">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($enquiries as $enquiry)
                <tr>
                    <th style="vertical-align: middle;text-align: center">{{$enquiry->id}}</th>
                    <td style="vertical-align: middle">{{ $enquiry->name }}</td>
                    <td style="vertical-align: middle">{{ $enquiry->email }}</td>
                    <td style="vertical-align: middle">{{ $enquiry->mobile }}</td>
                    <td style="vertical-align: middle">{{ $enquiry->message }}</td>
                    <td style="vertical-align: middle" align="center">
                        <form id="frm_{{$enquiry->id}}"
                              action="{{url('cp/enquiries/delete/'.$enquiry->id)}}"
                              method="post" style="padding-bottom: 0px;margin-bottom: 0px">
                            <input type="hidden" name="_method" value="delete"/>
                            {{csrf_field()}}
                            <a class="btn btn-danger btn-sm" title="Delete"
                               href="javascript:if(confirm('Are you sure want to delete?')) $('#frm_{{$enquiry->id}}').submit()">
                                Delete
                            </a>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">
                {{$enquiries->links('vendor.pagination.bootstrap-4')}}
            </ul>
        </nav>
    </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
@endsection