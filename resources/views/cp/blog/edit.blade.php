@extends('layouts.cp')
@section('title', 'Create New Blog Post')
@section('content')
<div class="row">
  <div class="col-lg-12">
  <h1>Edit Blog Post</h1>
 
 @if(Session::has('success'))
    <div class="alert alert-success">
      {{ Session::get('success') }}
    </div>
 @endif
  
 {!! Form::open(['route'=>'cp-blog-edit', 'files'=>'true']) !!}
  
 <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
 {!! Form::label('Title:') !!}
 {!! Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=>'Enter Blog Title']) !!}
 <span class="text-danger">{{ $errors->first('title') }}</span>
 </div>
  
 <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
 {!! Form::label('Description:') !!}
 {!! Form::textarea('description', old('description'), ['class'=>'form-control', 'placeholder'=>'Enter Blog Description']) !!}
 <span class="text-danger">{{ $errors->first('message') }}</span>
 </div>

 <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
 {!! Form::label('Upload Image:') !!}
 {!! Form::file('image', old('image'), ['class'=>'form-control', 'placeholder'=>'Blog Image']) !!}
 <span class="text-danger">{{ $errors->first('message') }}</span>
 </div>
  
 <div class="form-group">
 <button class="btn btn-success">Create Blog</button>
 {!! Form::hidden('created_by', Auth::user()->id) !!}
 </div>
  
 {!! Form::close() !!}
  </div>
</div>

@if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
@endif



@endsection
