@extends('layouts.cp')
@section('title', 'Posts')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="pull-left">
      <h2>Blog Posts</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-success" href="{{route('cp-blog-create')}}"> Create New Post</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
@endif

<table class="table table-bordered table-striped table-hover">
  <thead class="bg-primary">
    <tr>
      <th>No</th>
      <th>ID</th>
      <th width="300px">Title</th>
      <th>Description</th>
      <th width="200px">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($posts as $post)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $post->id}}</td>
      <td>{{ $post->title }}</td>
      <td>{{ $post->description }}</td>
      <td>
      
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $posts->links() }}
@endsection
