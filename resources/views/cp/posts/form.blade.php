@extends('layouts.cp')
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <h1>{{isset($post)?'Edit':'New'}} Blog Post</h1>
            <hr/>
            @if(isset($post))
                {!! Form::model($post,['method'=>'put']) !!}
            @else
                {!! Form::open(['files'=>'true']) !!}
            @endif
            <div class="form-group row required">
                {!! Form::label("title","Title",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                <div class="col-md-8">
                    {!! Form::text("title",null,["class"=>"form-control".($errors->has('title')?" is-invalid":""),"autofocus",'placeholder'=>'Enter Blog Title']) !!}
                    {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row required">
                {!! Form::label("description","Description",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                <div class="col-md-8">
                    {!! Form::textarea("description",null,["class"=>"form-control".($errors->has('description')?" is-invalid":""),'placeholder'=>'Enter Blog Description']) !!}
                    {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row required">
            {!! Form::label("image","Upload Image",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                <div class="col-md-8">
                    {!! Form::file("image",null,["class"=>"form-control".($errors->has('description')?" is-invalid":""),'placeholder'=>'Select Blog Image']) !!}
                    {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
           
            <div class="form-group row">
                <div class="col-md-3 col-lg-2"></div>
                <div class="col-md-4">
                    <a href="{{url('laravel-crud-search-sort')}}" class="btn btn-danger">
                        Back</a>
                    {!! Form::button("Save",["type" => "submit","class"=>"btn
                btn-primary"])!!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection