@extends('layouts.cp')
@section('title', 'Posts')
@section('content')
    <div class="container">
        <div class="text-right">
            <a href="{{url('cp/posts/create')}}" class="btn btn-primary">New Post</a>
        </div>
        <h1 style="font-size: 2.2rem">Posts</h1>
        <hr/>
        {!! Form::open(['method'=>'get']) !!}
        <div class="row">
            <!-- <div class="col-sm-4 form-group">
                {!! Form::select('gender',['-1'=>'Select Gender','Male'=>'Male','Female'=>'Female'],null,['class'=>'form-control','onChange'=>'form.submit()']) !!}
            </div> -->
            <div class="col-sm-5 form-group">
                <div class="input-group">
                    <input class="form-control" id="search"
                           value="{{ request('search') }}"
                           placeholder="Search title" name="search"
                           type="text" id="search"/>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-warning"
                        >
                            Search
                        </button>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{request('field')}}" name="field"/>
            <input type="hidden" value="{{request('sort')}}" name="sort"/>
        </div>
        {!! Form::close() !!}
        <table class="table table-bordered bg-light">
            <thead class="bg-primary" style="color: white">
            <tr>
                <th width="60px" style="vertical-align: middle;text-align: center">No</th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/posts')}}?search={{request('search')}}&field=name&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Title
                    </a>
                    {{request('field','name')=='name'?(request('sort','asc')=='asc'?'':''):''}}
                </th>
                <th style="vertical-align: middle">
                    <a href="{{url('cp/posts')}}?search={{request('search')}}&gender={{request('gender')}}&field=gender&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        Description
                    </a>
                    {{request('field')=='gender'?(request('sort','asc')=='asc'?'&#9652;':'&#9662;'):''}}
                </th>
                <th width="130px" style="vertical-align: middle">Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($posts as $post)
                <tr>
                    <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                    <td style="vertical-align: middle">{{ $post->title }}</td>
                    <td style="vertical-align: middle">{{ $post->description }}</td>
                    
                    <td style="vertical-align: middle" align="center">
                        <form id="frm_{{$post->id}}"
                              action="{{url('cp/posts/delete/'.$post->id)}}"
                              method="post" style="padding-bottom: 0px;margin-bottom: 0px">
                            <a class="btn btn-primary btn-sm" title="Edit"
                               href="{{url('cp/posts/update/'.$post->id)}}">
                                Edit</a>
                            <input type="hidden" name="_method" value="delete"/>
                            {{csrf_field()}}
                            <a class="btn btn-danger btn-sm" title="Delete"
                               href="javascript:if(confirm('Are you sure want to delete?')) $('#frm_{{$post->id}}').submit()">
                                Delete
                            </a>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">
                {{$posts->links('vendor.pagination.bootstrap-4')}}
            </ul>
        </nav>
    </div>
@endsection