Hello Sir,
<p>A new enquiry has been raised at one of your websites Sidak Singh.</p>
 
<p><u>following are the details submitted by the user:</u></p>

<div>
  <p><b>Name:</b>&nbsp;{{ $enquiry->name }}</p>
  <p><b>Email:</b>&nbsp;{{ $enquiry->email }}</p>
  <p><b>Mobile:</b>&nbsp;{{ $enquiry->mobile }}</p>
  <p><b>Message:</b>&nbsp;{{ $enquiry->message }}</p>
</div> 

Regards,
<i>Team Sidak Singh</i>