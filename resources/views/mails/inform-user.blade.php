Hello <i>{{ $enquiry->name }}</i>,
<p>Thanks for contacing us. We will get in touch with you shortly.</p>
 
<p><u>Here are the details that you sent:</u></p>

<div>
  <p><b>Name:</b>&nbsp;{{ $enquiry->name }}</p>
  <p><b>Email:</b>&nbsp;{{ $enquiry->email }}</p>
  <p><b>Mobile:</b>&nbsp;{{ $enquiry->mobile }}</p>
  <p><b>Message:</b>&nbsp;{{ $enquiry->message }}</p>
</div> 
<div>
  <p>If you have received this email by mistake, kindly ignore it</p>
</div>

Thank You,
<br/>
<i>Team Sidak Singh</i>