@extends('layouts.external')
@section('title', 'Blog - Sidak Singh')
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Blog</h2>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-6" style="margin-bottom:40px">
                <div class="panel" style="border:1px solid #F80; font-family:arial;min-height:80px;border-radius:3px;padding:20px;min-height:650px;">
                    <h3>{{$post->title}}</h3>
                    <p style="color:#888">Last updated on {{date_format($post->updated_at, 'F d, Y')}} by {{$post->user->name}}</p>
                    <img src="img/posts/{{$post->image}}" style="width:100%;margin-bottom:16px;" >
                    <p>{{$post->description}}</p>
                    <a class="btn btn-danger btn-block text-white" href="{{url('/blog') .'/'. $post->slug}}">Continue reading...</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
