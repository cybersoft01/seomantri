@extends('layouts.external')
@section('title', 'ppc packages in Delhi, India')
@section('content')
<section>
  <div class="container" style="min-height:500px">
    <div class="row">
      <div class="col-md-12">
        <h1 class="section-heading text-uppercase">ppc packages in Delhi, India</h1>
        <p class="seo">Boost the queries and sales by Advertising your business through PPC (Pay per Click). It recommends multiple solutions and approaches to appear on the most commonly used search engines list like Google, Yahoo, Bing and many others. It provides impressions and click only from targeted audience.</p>
      </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item list-group-item-info">
            <h4 class="list-group_item-heading text-center ">Small</h4>
          </li>
          <li href="#" class="list-group-item active">INR 10,000 / Month + 18% GST</li>
          <li class="list-group-item list-group-item-info">15 Keywords per month</li>
          <li class="list-group-item list-group-item-info">80% keywords top 10 Guarantee</li>
          <li class="list-group-item list-group-item-info">30 Directory submissions</li>
          <li class="list-group-item list-group-item-info">40 Social bookmarking</li>
          <li class="list-group-item list-group-item-info">5 Article Submission</li>
          <li class="list-group-item list-group-item-info">5 Press Release</li>
          <li class="list-group-item list-group-item-info">10 Profile Creation</li>
          <li class="list-group-item list-group-item-info">1 Blog</li>
         </ul> <br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
      <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item medium">
            <h2 class="list-group_item-heading text-center">Medium</h2>
          </li>
          <li href="#" class="list-group-item active" style="background-color:#0A0">INR 10,000 / Month + 18% GST</li>
          <li class="list-group-item medium">15 Keywords per month</li>
          <li class="list-group-item medium">80% keywords top 10 Guarantee</li>
          <li class="list-group-item medium">30 Directory submissions</li>
          <li class="list-group-item medium">40 Social bookmarking</li>
          <li class="list-group-item medium">5 Article Submission</li>
          <li class="list-group-item medium">5 Press Release</li>
          <li class="list-group-item medium">10 Profile Creation</li>
          <li class="list-group-item medium">1 Blog</li>
         </ul> <br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
      <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item list-group-item-info">
            <h4 class="list-group_item-heading text-center">Business</h4>
          </li>
          <li href="#" class="list-group-item active">INR 10,000 / Month + 18% GST</li>
          <li class="list-group-item list-group-item-info">15 Keywords per month</li>
          <li class="list-group-item list-group-item-info">80% keywords top 10 Guarantee</li>
          <li class="list-group-item list-group-item-info">30 Directory submissions</li>
          <li class="list-group-item list-group-item-info">40 Social bookmarking</li>
          <li class="list-group-item list-group-item-info">5 Article Submission</li>
          <li class="list-group-item list-group-item-info">5 Press Release</li>
          <li class="list-group-item list-group-item-info">10 Profile Creation</li>
          <li class="list-group-item list-group-item-info">1 Blog</li>
         </ul> <br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
    </div>
    
  </div>
</section>



@endsection
