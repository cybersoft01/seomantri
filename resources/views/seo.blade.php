@extends('layouts.external')
@section('title', 'Best SEO packages in Delhi, India')
@section('content')
<section>
  <div class="container" style="min-height:500px">
    <div class="row">
      <div class="col-md-12">
        <h1 class="section-heading text-uppercase">Best SEO packages in Delhi, India</h1>
        <p class="seo">Here are some of the best SEO packages in delhi India given below but firstly we should know what SEO is and how it works and why we need this for better outcome of our business.</p>
      
        <h4 class="section-heading ">What is SEO?</h4>
        <p class="seo">Search Engine Optimization (SEO) is well known for improving the rank of your website to generate more organic traffic on your website or business. This is the a simple definition of SEO by us.    </p>
        
        <h4 class="section-heading ">How SEO works?</h4>
        <p class="seo">SEO is a very complicated and intresting thing which consists of many small-small things like: On page optimization, Off page optimization, Creating backlinks, Social bookmarking, Article submission, brand awareness by posting comments and many more things else. </p>

        <h4 class="section-heading ">What is the use of SEO?</h4>
        <p class="seo">Search Engine Optimization (SEO) is well known for improving the rank of your website to generate more organic traffic on your website or business. This is the as simple definition of SEO by us.    </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item list-group-item-info">
            <h4 class="list-group_item-heading text-center ">Small</h4>
          </li>
          <li href="#" class="list-group-item active">INR 5,000 / Month + 18% GST</li>
          <li class="list-group-item list-group-item-info">05 Keywords per month</li>
          <li class="list-group-item list-group-item-info">80% keywords top 10 Guarantee</li>
          <li class="list-group-item list-group-item-info">20 Directory submissions</li>
          <li class="list-group-item list-group-item-info">25 Social bookmarking</li>
          <li class="list-group-item list-group-item-info">5 Article Submission</li>
          <li class="list-group-item list-group-item-info">4 Press Release</li>
          <li class="list-group-item list-group-item-info">10 Profile Creation</li>
          <li class="list-group-item list-group-item-info">2 Blog</li>
         </ul><br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
      <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item medium">
            <h2 class="list-group_item-heading text-center">Medium</h2>
          </li>
          <li href="#" class="list-group-item active" style="background-color:#0A0">INR 8,000 / Month + 18% GST</li>
          <li class="list-group-item medium">10 Keywords per month</li>
          <li class="list-group-item medium">80% keywords top 10 Guarantee</li>
          <li class="list-group-item medium">30 Directory submissions</li>
          <li class="list-group-item medium">35 Social bookmarking</li>
          <li class="list-group-item medium">5 Article Submission</li>
          <li class="list-group-item medium">5 Press Release</li>
          <li class="list-group-item medium">10 Profile Creation</li>
          <li class="list-group-item medium">3 Blog</li>
         </ul> <br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
      <div class="col-md-4 text-center">
        <ul class="list-group">
          <li class="list-group-item list-group-item-info">
            <h4 class="list-group_item-heading text-center">Business</h4>
          </li>
          <li href="#" class="list-group-item active">INR 12,000 / Month + 18% GST</li>
          <li class="list-group-item list-group-item-info">15 Keywords per month</li>
          <li class="list-group-item list-group-item-info">80% keywords top 10 Guarantee</li>
          <li class="list-group-item list-group-item-info">40 Directory submissions</li>
          <li class="list-group-item list-group-item-info">45 Social bookmarking</li>
          <li class="list-group-item list-group-item-info">5 Article Submission</li>
          <li class="list-group-item list-group-item-info">5 Press Release</li>
          <li class="list-group-item list-group-item-info">15 Profile Creation</li>
          <li class="list-group-item list-group-item-info">5 Blog</li>
         </ul> <br>
         <a class="btn btn-danger btn-lg text-white" href="tel:9911665256">Call us here:- 9911665256</a>
      </div>
    </div>
    
  </div>
</section>



@endsection
