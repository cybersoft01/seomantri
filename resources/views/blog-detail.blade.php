@extends('layouts.external')
@section('title', $post->slug)
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">{{$post->title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom:40px">
                <div class="panel" style="font-family:arial;min-height:80px;border-radius:3px;padding:20px;min-height:650px;">
                    <h3>{{$post->title}}</h3>
                    <p style="color:#888">Last updated on {{date_format($post->updated_at, 'F d, Y')}} by {{$post->user->name}}</p>
                    <img src="{{asset('img/posts')}}/{{$post->image}}" style="width:100%;margin-bottom:16px;" >
                    <p>{{$post->description}}</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
