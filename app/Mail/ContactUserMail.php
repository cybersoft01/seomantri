<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUserMail extends Mailable {
    use Queueable, SerializesModels;
    public $enquiry;
    public function __construct($enquiry) {
        $this->enquiry = $enquiry;
    }

    public function build() {
        return $this->from('cybersoftknp@gmail.com', 'Sidak Singh')
        ->subject("Thanks for contacting Sidak Singh")
        ->view('mails.inform-user');
    }
}