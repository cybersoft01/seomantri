<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactAdminMail extends Mailable
{
    use Queueable, SerializesModels;
    public $enquiry;
    public function __construct($enquiry) {
        $this->enquiry = $enquiry;
    }
    
    public function build()
    {
        return $this->from('registration@binimbibe.com', 'Sidak Singh')
        ->subject("A new enquiry has been generated at Sidak Singh")
        ->view('mails.inform-admin');
    }
}
