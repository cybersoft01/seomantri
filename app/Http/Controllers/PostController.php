<?php

namespace App\Http\Controllers;
use App\Blog;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        //$gender = $request->get('gender') != '' ? $request->get('gender') : -1;
        $field = $request->get('field') != '' ? $request->get('field') : 'name';
        $sort = $request->get('sort') != '' ? $request->get('sort') : 'asc';
        $posts = DB::table('blogs')->where('title', 'like', '%' . $search . '%')
        ->orWhere('description', 'like', '%' . $search . '%')
            ->orderBy($field, $sort)
            ->paginate(5)
            ->withPath('?search=' . $search . '&field=' . $field . '&sort=' . $sort);
        return view('cp.posts.index', compact('posts'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get'))
            return view('cp.posts.form');
        else {
            $rules = [
                'title' => 'required',
                'description' => 'required',
            ];
            $this->validate($request, $rules);
            $post = new Blog();
            $post->title = $request->title;
            $post->description = $request->description;
            $post->created_by = Auth::user()->id;
            if($request->hasFile('image')){
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
                ]);
                $imageName = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('img/posts'), $imageName);
                // $path = $request->file('image')->store('images/blogs');
                $post->image = $imageName;
            }
            $post->save();
            return redirect('/cp/posts');
        }
    }

    public function delete($id)
    {
        Blog::destroy($id);
        return redirect('/cp/posts');
    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('cp.posts.form', ['post' => Blog::find($id)]);
        else {
            $rules = [
                'title' => 'required',
                'description' => 'required',
            ];
            $this->validate($request, $rules);
            $post = Blog::find($id);
            $post->title = $request->title;
            $post->description = $request->description;
            
            $post->save();
            return redirect('/cp/posts');
        }
    }
}
