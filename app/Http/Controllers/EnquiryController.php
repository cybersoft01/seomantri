<?php

namespace App\Http\Controllers;

use App\Enquiry;
use App\Mail\ContactAdminMail;
use App\Mail\ContactUserMail;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Illuminate\Support\Facades\DB;

class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('contactUS');
    }
    
    public function list(Request $request)
    {
        $search = $request->get('search');
        $field = $request->get('field') != '' ? $request->get('field') : 'id';
        $sort = $request->get('sort') != '' ? $request->get('sort') : 'desc';
        $enquiries = DB::table('enquiries')
                    ->where('name', 'like', '%' . $search . '%' )
                    ->orWhere('email', 'like', '%' . $search . '%' )
                    ->orWhere('message', 'like', '%' . $search . '%' )
                    // $enquiries = $enquiries->where('name', 'like', '%' . $search . '%' )
        ->orderBy($field, $sort)
        ->paginate(15)
        ->withPath('?search=' . $search . '&field=' . $field . '&sort=' . $sort);
        return view('cp.enquiries.index', compact('enquiries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUSPost(Request $request)
   {
       $this->validate($request, [
        'name' => 'required|max:50',
        'email' => 'required|email',
        'mobile' => 'required|max:12',
        'message' => 'required'
        ]);
 
        Enquiry::create($request->all());

        // $objEnquiry = new \stdClass();
        // $objEnquiry->name = $request->get('name');
        

        // Mail::send('mails.email',
        //     array(
        //         'name' => $request->get('name'),
        //         'email' => $request->get('email'),
        //         'mobile' => $request->get('mobile'),
        //         'user_message' => $request->get('message')
        //     ), function($message)
        // {
        //     $message->to('singhsidak1994@gmail.com', 'Admin')->subject('New Enquiry generated at Sidak Singh\'s Website');
        // });

        Mail::to($request->get('email'))->send(new ContactUserMail($request));
        Mail::to('singhsidak1994@gmail.com')->send(new ContactAdminMail($request));
 
       return back()->with('success', 'Thanks for contacting us!');
       //return true;
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Enquiry $enquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enquiry $enquiry)
    {
        //
    }
}
