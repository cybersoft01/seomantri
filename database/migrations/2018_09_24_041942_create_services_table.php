<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Service;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->text('description');
            $table->unsignedInteger('created_by');
            $table->timestamps();
        });

        Service::create([
            'id' => '1',
            'title' => 'Search Engine Optimization (SEO)',
            'description' => "Looking for the optimized SEO services to make your website more search engine friendly or user friendly? We've got some affordable plans which will definately suits your budget.",
            'created_by' => '1'
        ]);

        Service::create([
            'id' => '2',
            'title' => 'Social Media Marketing & Optimization',
            'description' => "Nowadays approx 79% of our customer audience are active on Social Media platforms so if you want to increase your visibility on these platforms then we have some very good plans for you.",
            'created_by' => '1'
        ]);

        Service::create([
            'id' => '3',
            'title' => 'PPC (Adwords, Facebook, Instagram)',
            'description' => "These days advertisement is the major part for branding and lead creation, we provide impressions and clicks only from the targeted audience. We have packages for google, Facebook and Instagram also.",
            'created_by' => '1'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
