<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{slug}', 'BlogController@show');

Route::get('/best-seo-services-in-delhi', function () {
    return view('seo');
})->name('seo');

Route::get('/social-media-marketing-services-in-delhi', function () {
    return view('social');
})->name('social');

Route::get('/ppc-services-in-delhi', function () {
    return view('ppc');
})->name('ppc');

Route::get('/website-designing-in-delhi', function () {
    return view('web-design');
})->name('web-design');

Route::get('/website-development-in-delhi', function () {
    return view('web-develop');
})->name('web-develop');

Route::get('my-posts', 'PostController@myPosts');
Route::resource('posts','PostController');
Route::get('contact-us', 'EnquiryController@index');
Route::post('contact-us', ['as'=>'contactus.store','uses'=>'EnquiryController@contactUSPost']);

Route::group(['prefix' => 'cp/posts'], function () {
    Route::get('/', 'PostController@index');
    Route::match(['get', 'post'], 'create', 'PostController@create');
    Route::match(['get', 'put'], 'update/{id}', 'PostController@update');
    Route::delete('delete/{id}', 'PostController@delete');
});

Route::group(['prefix' => 'cp/enquiries'], function () {
    Route::get('/', 'EnquiryController@list');
    //Route::delete('delete/{id}', 'EnquiryController@delete');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cp', 'HomeController@index')->name('home');
Route::get('/cp/services', 'ServiceController@index')->name('cp-services');
Route::get('/cp/blog', 'BlogController@index')->name('cp-blog');
Route::get('/cp/blog/create', 'BlogController@create')->name('cp-blog-create');
Route::post('/cp/blog/create', 'BlogController@store')->name('cp-blog-store');
